﻿using System;
using System.Collections.Generic;

namespace BabySitter
{
    public class PaymentCalculator
    {
        private readonly int _beforeBedPay = 12;
        private readonly int _afterBedPay = 8;
        private readonly int _afterMidnightPay = 16;

        // Calculates the amount of pay for one night of work
        public int CalculatePay(string startTime, string endTime, string bedTime)
        {
            // convert and round all times
            var time = FormatTime(startTime, endTime, bedTime);

            // Calculate pay for each payrate
            var beforeBedPay = GetBeforeBedHours(time) * _beforeBedPay;
            var afterBedPay = GetAfterBedHours(time) * _afterBedPay;
            var afterMidnightPay = GetAfterMidnightHours(time) * _afterMidnightPay;

            return beforeBedPay + afterBedPay + afterMidnightPay;
        }

        public Dictionary<string, int> FormatTime(string startTime, string endTime, string bedTime)
        {
            var time = new Dictionary<string, int>();
            var endTimeHour = int.Parse(endTime.Split(':')[0]);
            var endTimeMinutes = int.Parse(endTime.Split(':')[1]);

            // Split and convert military time to int
            time.Add("startTime", int.Parse(startTime.Split(':')[0]));
            time.Add("bedTime", int.Parse(bedTime.Split(':')[0]));

            // Add one hour to end time if partial hour for full hour billing
            if (endTimeMinutes > 0 && endTimeHour == 24)
            {
                time.Add("endTime", 1);
            }
            else if (endTimeMinutes > 0)
            {
                time.Add("endTime", endTimeHour + 1);
            }
            else
            {
                time.Add("endTime", endTimeHour);
            }

            return time;
        }

        // Gets hours worked before bed time
        public int GetBeforeBedHours(Dictionary<string, int> time)
        {
            return time["bedTime"] - time["startTime"];
        }
        
        // Gets hours worked after bed time
        public int GetAfterBedHours(Dictionary<string, int> time)
        {
            if (time["endTime"] <= 24 && time["endTime"] > time["bedTime"])
            {
                return time["endTime"] - time["bedTime"];
            }
            else if (time["endTime"] <= 4)
            {
                return 24 - time["bedTime"];
            }
            return 0;
        }

        // Gets hours worked after midnight
        public int GetAfterMidnightHours(Dictionary<string, int> time)
        {
            return time["endTime"] >= 17 ? 0 : time["endTime"];
        }

    }
}
