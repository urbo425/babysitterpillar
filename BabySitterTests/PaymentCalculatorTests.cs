﻿using System.Collections.Generic;
using BabySitter;
using NUnit.Framework;

namespace BabySitterTests
{
    [TestFixture]
    public class PaymentCalculatorTests
    {
        private PaymentCalculator _paymentCalculator;
        [SetUp]
        public void Setup()
        {
            _paymentCalculator = new PaymentCalculator();
        }

        [Test]
        public void CanCalculatePaymentForBeforeBedTime()
        {
            var totalPay = _paymentCalculator.CalculatePay("17:00", "20:00", "20:00");
            Assert.That(totalPay, Is.EqualTo(36));
        }

        [Test]
        public void CanCalculatePaymentForAfterBedTime()
        {
            var totalPay = _paymentCalculator.CalculatePay("20:00", "23:00", "20:00");
            Assert.That(totalPay, Is.EqualTo(24));
        }

        [Test]
        public void CanCalculatePaymentForAfterMidnightPay()
        {
            var totalPay = _paymentCalculator.CalculatePay("24:00", "1:00", "24:00");
            Assert.That(totalPay, Is.EqualTo(16));
        }

        [Test]
        public void CanCalculatePaymentForBeforeAndAfterBedTime()
        {
            var totalPay = _paymentCalculator.CalculatePay("17:00", "19:00", "18:00");
            Assert.That(totalPay, Is.EqualTo(20));
        }
        
        [Test]
        public void CanCalculatePaymentForAllThreePaymentTypes()
        {
            var totalPay = _paymentCalculator.CalculatePay("22:00", "1:00", "23:00");
            Assert.That(totalPay, Is.EqualTo(36));
        }

        [Test]
        public void CanCalculatePaymentForPartialBeforeBedPay()
        {
            var totalPay = _paymentCalculator.CalculatePay("22:20", "2:00", "23:00");
            Assert.That(totalPay, Is.EqualTo(52));
        }

        [Test]
        public void CanCalculatePaymentForPartialAfterBedPay()
        {
            var totalPay = _paymentCalculator.CalculatePay("22:00", "23:30", "23:00");
            Assert.That(totalPay, Is.EqualTo(20));
        }

        [Test]
        public void CanCalculatePaymentForPartialAfterMidnightPay()
        {
            var totalPay = _paymentCalculator.CalculatePay("22:00", "1:30", "23:00");
            Assert.That(totalPay, Is.EqualTo(52));
        }

        [Test]
        public void CanCalculatePaymentForPartialBedTimePay()
        {
            var totalPay = _paymentCalculator.CalculatePay("22:00", "1:30", "23:00");
            Assert.That(totalPay, Is.EqualTo(52));
        }

        [Test]
        public void CanCreateTimeDictionary()
        {
            var time = _paymentCalculator.FormatTime("17:00", "1:00", "20:00");
            var timeTest = new Dictionary<string, int>
            {
                { "startTime", 17 },
                { "bedTime", 20 },
                { "endTime", 1 }
            };


            Assert.That(time, Is.EqualTo(timeTest));
        }
    }
}
